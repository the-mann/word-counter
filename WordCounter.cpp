


#include <iostream>
#include <fstream>
#include <string>
#include <functional>
#include <algorithm>
#include <chrono>

using namespace std;
using namespace std::chrono;

#include "RBTree.h"

int main(int argv, char *argc[]) {
    //start clock to measure how long it takes.
    auto start = high_resolution_clock::now();

    RBTree lbt;
    string word, lastword;

    // File input.
    // checks to make sure there are at least two arguments: one for the output file and a 1 input file min
    if (argv < 2) {
        cerr << "no input file specified" << endl;
        system("pause");
        exit(1);
    }
    // define output file

    for (int count = 2; count < argv; count++) {
        ifstream input(argc[count]);
        if (!input) {
            cerr << "Cannot open input file" << argc[count] << endl;
            system("pause");
            exit(1);
        }
        while (input >> word) {
            std::transform(word.begin(), word.end(), word.begin(), ::tolower);
            word.erase(remove_if(word.begin(), word.end(), ::ispunct), word.end());

            lbt.insert(word);
        }
    }
    ofstream output(argc[1]);
    // We check just in case there is some weird permissions error.
    if (!output) {
        cerr << "Cannout open output file " << argc[1] << endl;
        system("pause");
        exit(1);
    }
    //Now we print out results...
    string final = lbt.inorder();
    // We also see if the word "the" is in the file - this is just to test the find method of RBTree.
    Node test = lbt.find("the");
    output << "testing the find function:\n";
    if (!test.word.empty()) {
        output << "we found the word \"the\" - it was used " << test.count << " times\n\n";
    } else {
        output << "we didn't find the word \"the\"\n\n";
    }


    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);
    output << "It took " << duration.count() / 1000.0 << "s to finish.\n";
    output << final;
    return 0;
}