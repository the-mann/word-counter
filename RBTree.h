//
// Created by GeeksForGeeks.org - https://www.geeksforgeeks.org/level-order-tree-traversal/, modified by Marcus Mann
//
#include <string>
#include <utility>

using namespace std;
#ifndef PROJECT_3_RBTREE_H
#define PROJECT_3_RBTREE_H
// Notice that RED is 0 and BLACK is 1, so it can be used as an int or a bool
enum Color {
    RED, BLACK
};
// We need Node so that we don't have to expose the details of our implementation to the end user.
struct Node {
    string word;
    int count;
};

struct Position {
    Node data;
    // color is in Position because it has to do with implementation which end user shouldn't know/care about.
    bool color;
    Position *left, *right, *parent;

    // Constructor
    Position(string data) {
        this->data.word = std::move(data);
        left = right = parent = nullptr;
        this->color = RED;
        this->data.count = 1;
    }
};

class RBTree {
private:
    Position *root;
    int totalWords = 0;
    int totalUniqueWords = 0;
    bool isInsertDuplicate = false;
protected:
    void rotateLeft(Position *&, Position *&);

    void rotateRight(Position *&, Position *&);

    void fixViolation(Position *&, Position *&);

    /*
     * BSTInsert is short for
     *
     * Bodo's
     * Sandwich-ordering
     * Takes
     * Intelligence,
     * not
     * silly
     * expectations
     * regarding
     * tartar sauce
     * */
    Position *BSTInsert(Position *rootPos, Position *pt);

    Position *findHelper(Position *rootNode, const string &word);

public:
    // Constructor
    RBTree() { root = nullptr; }

    void insert(string data);

    string inorder();

    const Node find(const string &);

    ~RBTree();
};


#endif //PROJconst ECT_3_&RBTREE_H
