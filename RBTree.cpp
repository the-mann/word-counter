//
// Created by GeeksForGeeks.org - https://www.geeksforgeeks.org/level-order-tree-traversal/, modified by Marcus Mann
//

using namespace std;

#include <iostream>
#include <queue>
#include <utility>
#include "RBTree.h"

// A recursive function to do level order traversal
string inorderHelper(Position *rootPos, string &newStr) {
    if (rootPos == nullptr || rootPos->data.word.empty()) {
        newStr += "";
    } else {
        inorderHelper(rootPos->left, newStr);
        newStr += rootPos->data.word + "  " + to_string(rootPos->data.count) + '\n';
        inorderHelper(rootPos->right, newStr);
    }
    return newStr;
}

// This is essentially a postorder traversal so the last thing deleted is the root node.
void destructorHelper(Position *rootPos) {
    if (rootPos != nullptr) {
        destructorHelper(rootPos->left);
        destructorHelper(rootPos->right);
        delete rootPos;
    }
}

/* A utility function to insert a new node with given key
   in BST */
Position *RBTree::BSTInsert(Position *rootPos, Position *pt) {
    /* If the tree is empty, return a new node */
    if (rootPos == nullptr) {
        totalUniqueWords++;
        totalWords++;
        return pt;
    }

    // Duplicate word, just increase count and set the duplicate flag.
    if (pt->data.word == rootPos->data.word) {
        rootPos->data.count++;
        totalWords++;
        isInsertDuplicate = true;
    }
        /* Otherwise, recur down the tree */
    else if (pt->data.word.compare(rootPos->data.word) < 0) {
        rootPos->left = BSTInsert(rootPos->left, pt);
        rootPos->left->parent = rootPos;
    } else if (pt->data.word.compare(rootPos->data.word) > 0) {
        rootPos->right = BSTInsert(rootPos->right, pt);
        rootPos->right->parent = rootPos;
    }

    /* return the (unchanged) node pointer */
    return rootPos;
}

// Utility function to do level order traversal
void levelOrderHelper(Position *rootPos) {
    if (rootPos == nullptr)
        return;

    std::queue<Position *> q;
    q.push(rootPos);

    while (!q.empty()) {
        Position *temp = q.front();
        cout << temp->data.word << "  ";
        q.pop();

        if (temp->left != nullptr)
            q.push(temp->left);

        if (temp->right != nullptr)
            q.push(temp->right);
    }
}

void RBTree::rotateLeft(Position *&rootPos, Position *&pt) {
    Position *pt_right = pt->right;

    pt->right = pt_right->left;

    if (pt->right != nullptr)
        pt->right->parent = pt;

    pt_right->parent = pt->parent;

    if (pt->parent == nullptr)
        rootPos = pt_right;

    else if (pt == pt->parent->left)
        pt->parent->left = pt_right;

    else
        pt->parent->right = pt_right;

    pt_right->left = pt;
    pt->parent = pt_right;
}

void RBTree::rotateRight(Position *&rootPos, Position *&pt) {
    Position *pt_left = pt->left;

    pt->left = pt_left->right;

    if (pt->left != nullptr)
        pt->left->parent = pt;

    pt_left->parent = pt->parent;

    if (pt->parent == nullptr)
        rootPos = pt_left;

    else if (pt == pt->parent->left)
        pt->parent->left = pt_left;

    else
        pt->parent->right = pt_left;

    pt_left->right = pt;
    pt->parent = pt_left;
}

// This function fixes violations caused by BST insertion
void RBTree::fixViolation(Position *&rootPos, Position *&pt) {

    Position *parent_pt = nullptr;
    Position *grand_parent_pt = nullptr;

    if (pt->parent != nullptr) {
        while ((pt->data.word != root->data.word) && (pt->color != BLACK) &&
               (pt->parent->color == RED)) {

            parent_pt = pt->parent;
            grand_parent_pt = pt->parent->parent;

            /*  Case : A
                Parent of pt is left child of Grand-parent of pt */
            if (parent_pt == grand_parent_pt->left) {

                Position *uncle_pt = grand_parent_pt->right;

                /* Case : 1
                   The uncle of pt is also red
                   Only Recoloring required */
                if (uncle_pt != nullptr && uncle_pt->color == RED) {
                    grand_parent_pt->color = RED;
                    parent_pt->color = BLACK;
                    uncle_pt->color = BLACK;
                    pt = grand_parent_pt;
                } else {
                    /* Case : 2
                       pt is right child of its parent
                       Left-rotation required */
                    if (pt == parent_pt->right) {
                        rotateLeft(rootPos, parent_pt);
                        pt = parent_pt;
                        parent_pt = pt->parent;
                    }

                    /* Case : 3
                       pt is left child of its parent
                       Right-rotation required */
                    rotateRight(rootPos, grand_parent_pt);
                    swap(parent_pt->color, grand_parent_pt->color);
                    pt = parent_pt;
                }
            }

                /* Case : B
                   Parent of pt is right child of Grand-parent of pt */
            else {
                Position *uncle_pt = grand_parent_pt->left;

                /*  Case : 1
                    The uncle of pt is also red
                    Only Recoloring required */
                if ((uncle_pt != nullptr) && (uncle_pt->color == RED)) {
                    grand_parent_pt->color = RED;
                    parent_pt->color = BLACK;
                    uncle_pt->color = BLACK;
                    pt = grand_parent_pt;
                } else {
                    /* Case : 2
                       pt is left child of its parent
                       Right-rotation required */
                    if (pt == parent_pt->left) {
                        rotateRight(root, parent_pt);
                        pt = parent_pt;
                        parent_pt = pt->parent;
                    }

                    /* Case : 3
                       pt is right child of its parent
                       Left-rotation required */
                    rotateLeft(rootPos, grand_parent_pt);
                    swap(parent_pt->color, grand_parent_pt->color);
                    pt = parent_pt;
                }
            }
        }
    }
    rootPos->color = BLACK;
}

// Function to insert a new node with given word
void RBTree::insert(string data) {
    Position *pt = new Position(std::move(data));

    // Do a normal BST insert
    root = BSTInsert(root, pt);
    // fix Red Black Tree violations
    if (isInsertDuplicate) {
        delete pt;
        isInsertDuplicate = false;
    } else {
        fixViolation(root, pt);
    }
}

// Function to do inorder and level order traversals
string RBTree::inorder() {
    string newStr;
    newStr += "Total number of words: " + to_string(totalWords) + "\nTotal number of different Words: " +
              to_string(totalUniqueWords) + '\n';
    inorderHelper(root, newStr);
    return newStr;
}

RBTree::~RBTree() {
    destructorHelper(root);
}

const Node RBTree::find(const string &word) {
    return findHelper(root, word)->data;
}

Position *RBTree::findHelper(Position *rootNode, const string &word) {
    Position *foundNode = nullptr;
//    cout << "node word: " << rootNode->word.word << '\n';
    if (word == rootNode->data.word) {
        return rootNode;
    }
        /* Otherwise, recur down the tree */
    else if (word.compare(rootNode->data.word) < 0) {
//        cout << word << " is going left to word: " << rootNode->left->word.word << '\n';
        foundNode = findHelper(rootNode->left, word);
    } else if (word.compare(rootNode->data.word) > 0) {
//        cout << word << " is going right to word: " << rootNode->right->word << '\n';
        foundNode = findHelper(rootNode->right, word);
    }

    /* return the (unchanged) node pointer */
    return foundNode;
}
